# Memory Game

A simple memory puzzle game using python and pygame modules.
A 4 x 4 grid containing 8 shapes of different colors will appear, and then disappear after a few seconds. Your goal is to get all the shapes back in their correct positions from memory.

# Instructions to Run

1. Download the required libraries

pip install -r requirements.txt

2. Run command

python memory_game.py

<img src = "images/img1.png">    
<img src = "images/img2.png">
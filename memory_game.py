import pygame
import sys
from pygame.locals import *
import random
import drawShapes

rectangles = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
shapesArrangement = []

Rectangle1 = Rect(0, 0, 100, 100)
Rectangle2 = Rect(100, 0, 100, 100)
Rectangle3 = Rect(200, 0, 100, 100)
Rectangle4 = Rect(300, 0, 100, 100)
Rectangle5 = Rect(0, 100, 100, 100)
Rectangle6 = Rect(100, 100, 100, 100)
Rectangle7 = Rect(200, 100, 100, 100)
Rectangle8 = Rect(300, 100, 100, 100)
Rectangle9 = Rect(0, 200, 100, 100)
Rectangle10 = Rect(100, 200, 100, 100)
Rectangle11 = Rect(200, 200, 100, 100)
Rectangle12 = Rect(300, 200, 100, 100)
Rectangle13 = Rect(0, 300, 100, 100)
Rectangle14 = Rect(100, 300, 100, 100)
Rectangle15 = Rect(200, 300, 100, 100)
Rectangle16 = Rect(300, 300, 100, 100)

RectangleObjects = (Rectangle1, Rectangle2, Rectangle3, Rectangle4, Rectangle5, Rectangle6, Rectangle7, Rectangle8, Rectangle9, Rectangle10, Rectangle11, Rectangle12, Rectangle13, Rectangle14, Rectangle15, Rectangle16)

def drawBackground():
    Window.fill((255, 255, 255))
    for i in range(16):
        pygame.draw.rect(Window, (255, 0, 0), RectangleObjects[i], 10)

def StartGame():
    
    for i in range(4):
        if i == 0 or i == 1:
            color = (0, 255, 255)
        else:
            color = (0, 255, 0)

        x = random.choice(rectangles)
        shapesArrangement.append(x)
        drawShapes.drawRecShapes(Window, x, color)
        rectangles.remove(x)

    for i in range(4):
        if i == 0 or i == 1:
            color = (255, 255, 0)
        else:
            color = (0, 255, 255)

        x = random.choice(rectangles)
        shapesArrangement.append(x)
        drawShapes.drawCirShapes(Window, x, color)
        rectangles.remove(x)
	
    for i in range(4):
        if i == 0 or i == 1:
            color = (255, 0, 255)
        else:
            color = (128, 0, 0)

        x = random.choice(rectangles)
        shapesArrangement.append(x)
        drawShapes.drawTriShapes(Window, x, color)
        rectangles.remove(x)

    for i in range(4):
        if i == 0 or i == 1:
            color = (128, 128, 0)
        else:
            color = (128, 0, 128)

        x = random.choice(rectangles)
        shapesArrangement.append(x)
        drawShapes.drawDiamShapes(Window, x, color)
        rectangles.remove(x)

def rightChoice(firstChoice, secondChoice) :
    
    x = shapeIndex(firstChoice)
    y = shapeIndex(secondChoice)

    if (x == 0 and y == 1 or x == 1 and y == 0) or (x == 2 and y == 3 or x == 3 and y == 2) or (x == 6 and y == 7 or x == 7 and y == 6) or (x == 4 and y == 5 or x == 5 and y == 4) or (x == 8 and y == 9 or x == 9 and y == 8) or (x == 10 and y == 11 or x == 11 and y == 10) or (x == 14 and y == 15 or x == 15 and y == 14) or (x == 12 and y == 13 or x == 13 and y == 12):
            return True

def shapeIndex(mousePosition) :
    
    for i in range(16):
        if RectangleObjects[i].collidepoint(mousePosition):
            return shapesArrangement.index(i + 1)

def recNumber(mousePosition) :
    
    for i in range(16):
        if RectangleObjects[i].collidepoint(mousePosition) :
            return i + 1

def hide(mousePosition) :
    
    rectangleNumber = recNumber(mousePosition)
    for i in range(16) :
        if rectangleNumber == i + 1 :
            pygame.draw.rect(Window, (255, 255, 255), RectangleObjects[i].inflate(-10, -10))

def show(mousePosition) :
    
    rectangleNumber = recNumber(mousePosition)
    index = shapeIndex(mousePosition)
    
    if index == 0 or index == 1:
        color = (0, 0, 255)
        drawShapes.drawRecShapes(Window, rectangleNumber, color)
    
    if index == 2 or index == 3:
        color = (0, 255, 0)
        drawShapes.drawRecShapes(Window, rectangleNumber, color)
    
    if index == 5 or index == 4:
        color = (255, 255, 0)
        drawShapes.drawCirShapes(Window, rectangleNumber, color)
    
    if index == 6 or index == 7:
        color = (0, 255, 255)
        drawShapes.drawCirShapes(Window, rectangleNumber, color)
    
    if index == 9 or index == 8:
        color = (255, 0, 255)
        drawShapes.drawTriShapes(Window, rectangleNumber, color)
    
    if index == 10 or index == 11:
        color = (128, 0, 0)
        drawShapes.drawTriShapes(Window, rectangleNumber, color)
    
    if index == 13 or index == 12:
        color = (128, 128, 0)
        drawShapes.drawDiamShapes(Window, rectangleNumber, color)
    
    if index == 14 or index == 15:
        color = (128, 0, 128)
        drawShapes.drawDiamShapes(Window, rectangleNumber, color)

def main():
    
    pygame.init()
    global Window
    Window = pygame.display.set_mode((410, 410))
    Window.fill((255, 255, 255))
    #Window.display.update()
    
    #Window.display.set_caption("First Game")
    drawBackground()
    StartGame()

    pygame.display.update()
    pygame.time.wait(3000)
    drawBackground()
    
    pygame.display.update()
    flag = 0
    choices = []

    while True:
        for event in pygame.event.get():
            
            if event.type == QUIT :
                pygame.quit()
                sys.exit()

            if event.type == pygame.MOUSEBUTTONUP :
                mousePosition = pygame.mouse.get_pos()
                show(mousePosition)
                pygame.display.update()
            
                if flag == 0 :
                    
                    firstChoice = mousePosition
                    
                    if recNumber(firstChoice) in choices :
                        flag = 0
                    else :
                        flag = 1

                else :
                    secondChoice = mousePosition

                    if recNumber(secondChoice) in choices :
                        flag = 1
                    else :
                        flag = 0

                    if not(recNumber(firstChoice) in choices) and not recNumber(secondChoice) in choices :
                        
                        if rightChoice(firstChoice, secondChoice) :
                            choices.append(recNumber(firstChoice))
                            choices.append(recNumber(secondChoice))
                        else :
                            pygame.time.wait(500)
                            hide(firstChoice)
                            hide(secondChoice)
                            pygame.display.update()

        if len(choices) == 16 :
            
            myFont = pygame.font.SysFont('Comic Sans MS', 60, True)
            winText = myFont.render('You did it', False, (0, 0, 0))

            Window.blit(winText, (20, 150))
            image = pygame.image.load('won.png')
    
            Window.blit(image, (10, 10))
            pygame.display.update()


if __name__ == "__main__":
    main()

